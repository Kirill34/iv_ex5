function getCountries()
{
    $.ajax({
        url: '/getCountries.php',         /* Куда пойдет запрос */
        method: 'get',             /* Метод передачи (post или get) */
        dataType: 'html',          /* Тип данных в ответе (xml, json, script, html). */
        data: {},     /* Параметры передаваемые в запросе. */
        success: function(data){   /* функция которая будет выполнена после успешного запроса.  */
            var arr=JSON.parse(data);
            var text="";
            arr.forEach(function(item, i, arr) {
                text+=item+"<br>";
            });
            $("#county_list").html(text);           /* В переменной data содержится ответ от index.php. */
        }
    })
}
function addCountry()
{
    $.post("/addCountry.php",$("#addCountry").serialize(),function (data){})
    document.getElementById("contry").value="";
}